# Team #wardriving
An unofficial repo with graphics and quick links. 

## Team Gear
- [Apparel](https://terminal-junkie.creator-spring.com/)
- [Jhewitt Wardriver Kit by 463n7](https://463n7.io/)
- [Sticker Pack](https://ftsedc.bigcartel.com/product/wardriving-sticker-set)
## Graphical Assets
All artwork is open-source for personal and commercial use

##### Yagi Tank
- [Yagi Tank + #wardriving](https://gitlab.com/terminaljunkie/wardriving/-/blob/main/graphics/Yagitankwardriving__1_.png?ref_type=heads)
- [Yagi Tank](https://gitlab.com/terminaljunkie/wardriving/-/blob/main/graphics/yagitank.png?ref_type=heads)

##### Emblem
- [Emblem Black](https://gitlab.com/terminaljunkie/wardriving/-/blob/main/graphics/emblemblack.jpg?ref_type=heads)
- [Emblem White](https://gitlab.com/terminaljunkie/wardriving/-/blob/main/graphics/emblemwhite.jpg?ref_type=heads)

##### Metal
Black metal stylized #wardriving graphic. Least to most "brutal"
- [V1 Black](https://gitlab.com/terminaljunkie/wardriving/-/blob/main/graphics/metal/v1black.png?ref_type=heads)
- [V1 White](https://gitlab.com/terminaljunkie/wardriving/-/blob/main/graphics/metal/v1white.png?ref_type=heads)
- [V1 Green](https://gitlab.com/terminaljunkie/wardriving/-/blob/main/graphics/metal/v1green.png?ref_type=heads)
- [V2 Black](https://gitlab.com/terminaljunkie/wardriving/-/blob/main/graphics/metal/v2black.png?ref_type=heads)
- [V2 White](https://gitlab.com/terminaljunkie/wardriving/-/blob/main/graphics/metal/v2white.png?ref_type=heads)
- [V2 Green](https://gitlab.com/terminaljunkie/wardriving/-/blob/main/graphics/metal/v2green.png?ref_type=heads)
- [V3 Black](https://gitlab.com/terminaljunkie/wardriving/-/blob/main/graphics/metal/v3black.png?ref_type=heads)
- [V3 White](https://gitlab.com/terminaljunkie/wardriving/-/blob/main/graphics/metal/v3white.png?ref_type=heads)
- [V3 Green](https://gitlab.com/terminaljunkie/wardriving/-/blob/main/graphics/metal/v3green.png?ref_type=heads)




# Sponsors & Member Sites

- [h-i-r.net](https://www.h-i-r.net/)
- [463n7.io](https://463n7.io/)
- [justcallmekokollc.com](https://www.justcallmekokollc.com/)
- [ftsedc.com](https://ftsedc.bigcartel.com/)






